﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwilioBindings.ViewModels;
using Xamarin.Forms;

namespace TwilioBindings
{
	public partial class MainPage : ContentPage
	{        
        public MainPage()
		{
			InitializeComponent();
            
            BindingContext = new MainPageViewModel();
        }
	}
}
