﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TwilioBindings.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private readonly string 
            TEXT_PROPERTY = "Text",
            VERSION_PREFIX = "Twilio Programmable Voice version: ";
        private string _version;

        public event PropertyChangedEventHandler PropertyChanged;

        public string Version
        {
            get
            {
                return _version;
            }
            set
            {
                _version = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(TEXT_PROPERTY));
            }
        }

        public MainPageViewModel()
        {
            Version = VERSION_PREFIX + TwilioPath.Voice.Version;
        }
    }
}
